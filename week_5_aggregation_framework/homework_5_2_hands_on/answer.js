db.grades.aggregate([
	{$match:
		{$or:[
			{state:"NY"},
			{state:"CA"}],
		pop:{$gt:25000}
		}
	},
	{$group: 
		{ _id: 
			{
			state:"$state", 
			city:"$city"
			},
		pop:{$sum:"$pop"}
		}
	}, 
	{$group: 
		{ _id:0,
		avg_pop:{$avg:"$pop"}
		}
	},
])
