db.zips.aggregate([
    {$project: 
     {
		first_char: {$substr : ["$city",0,1]},
		pop: 1
     }	 
   },
   {$group: 
		{ 
			_id: "$first_char",
			sum_population:{$sum:"$pop"}
		}
	},
	{$sort: 
		{
			_id: 1
		}
	},
	{$limit: 10},
	{
		$group: 
		{ 
			_id: 0,
			sum_population:{$sum:"$sum_population"}
		}
	}
])