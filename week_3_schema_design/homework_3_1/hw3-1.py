import pymongo
import sys

# establish a connection to the database
connection = pymongo.Connection("mongodb://localhost", safe=True)

# get a handle to the school database
db=connection.school
students = db.students


def find():

    print "Start find()"

    n = students.count()

    for x in range (0,200):
        student = students.find_one({'_id': x})
        min_score = 100
        min_homework = ''
        for score in student['scores']:
            if (score['type'] == 'homework'):
                if (min_score > score['score']):
                    min_score = score['score']
                    min_homework = score

        students.update({'_id':x},{'$pull' : {'scores': min_homework }})

find()