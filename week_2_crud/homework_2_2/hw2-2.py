import pymongo
import sys

# establish a connection to the database
connection = pymongo.Connection("mongodb://localhost", safe=True)

# get a handle to the school database
db=connection.students
grades = db.grades


def find():

    print "Start find()"

    query = {'type':'homework'}
    #selector = {'student_id':1, '_id':0}

    try:
        iter = grades.find(query).sort([('student_id',pymongo.ASCENDING),('score',pymongo.ASCENDING)])
        current_student_id = -1
        
        for doc in iter:
            if (current_student_id != doc.values()[0]):
                current_student_id = doc.values()[0]
                query = {'_id' : doc.values()[1]}
                try:
                    grades.remove(query)
                    if (current_student_id < 10):
                        print doc
                except AttributeError, e:
                    print e

    except AttributeError, e:
        print e

find()