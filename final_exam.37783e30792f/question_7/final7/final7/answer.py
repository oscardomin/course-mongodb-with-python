import pymongo

def delete_orphans():
    connection = pymongo.Connection("mongodb://localhost",safe=True)    
    db = connection.pinterest

    # let's get ourselves a sequence number
    ids = db.images.find({})


    for identifier in ids:
        query = {'images': identifier['_id']}
        if db.albums.find(query).count() <= 0 :
            db.images.remove({'_id':identifier['_id']})

delete_orphans()